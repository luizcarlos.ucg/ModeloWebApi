﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marmitinha.Persistencia
{
    public class Pedido
    {
        public async Task<List<Models.Pedido>> ObtenhaTodos()
        {
            return await Task.FromResult(ListaPedidosMock());
        }

        public async Task<Models.Pedido> Obtenha(int id)
        {
            return await Task.FromResult(ListaPedidosMock().Find(x => x.Id.Equals(id)));
        }

        public async Task<bool> Exclua(int id)
        {
            return true;
        }

        public async Task<int> Inclua(Models.Pedido usuario)
        {
            return 1;
        }

        public async Task<bool> Altere(Models.Pedido usuario)
        {
            return true;
        }

        private List<Models.Pedido> ListaPedidosMock()
        {
            return new List<Models.Pedido>
            {
                new Models.Pedido()
                {
                    Id = 1,
                    DataHoraFechamento = DateTime.UtcNow,
                    DataHoraEntrega = DateTime.UtcNow,
                    LatitudeEntrega = 234,
                    LongitudeEntrega = 123,
                    DeveEntregar = true,
                    Cancelado = false,
                    DataHoraGravacao = DateTime.UtcNow,
                    DataAtualizacao = DateTime.UtcNow
                },
                new Models.Pedido()
                {
                    Id = 1,
                    DataHoraFechamento = DateTime.UtcNow,
                    DataHoraEntrega = DateTime.UtcNow,
                    LatitudeEntrega = 2987,
                    LongitudeEntrega = 65464,
                    DeveEntregar = true,
                    Cancelado = false,
                    DataHoraGravacao = DateTime.UtcNow,
                    DataAtualizacao = DateTime.UtcNow
                }
            };
        }
    }
}