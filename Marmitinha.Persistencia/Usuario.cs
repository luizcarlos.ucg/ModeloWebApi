﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marmitinha.Persistencia
{
    public class Usuario
    {
        public async Task<List<Models.Usuario>> ObtenhaTodos()
        {
            return await Task.FromResult(ListaUsuariosMock());
        }

        public async Task<Models.Usuario> Obtenha(int id)
        {
            return await Task.FromResult(ListaUsuariosMock().Find(x => x.Id.Equals(id)));
        }

        public async Task<bool> Exclua(int id)
        {
            return true;
        }

        public async Task<int> Inclua(Models.Usuario usuario)
        {
            return 1;
        }

        public async Task<bool> Altere(Models.Usuario usuario)
        {
            return true;
        }

        private List<Models.Usuario> ListaUsuariosMock()
        {
            return new List<Models.Usuario>
            {
                new Models.Usuario()
                {
                    Id = 5,
                    Nome = "Luiz Carlos Machado",
                    Latitude = 123,
                    Longitude = 456,
                    Foto = "123456",
                    Identificador = "luizcarlos.ucg@gmail.com",
                    Rua = "Bougainville",
                    Bairro = "Residencial dos Ipês",
                    Cidade = "Goiânia",
                    Estado = "GO",
                    CpfCnpj = "451.123.586-45",
                    Telefone = "+5562991512536",
                    DataAtualizacao = DateTime.UtcNow,
                    TipoCadastro = Models.Enum.ETipoCadastro.Email,
                    TipoUsuario = Models.Enum.ETipoUsuario.Cliente
                },
                new Models.Usuario()
                {
                    Id = 8,
                    Nome = "Maria Aparecida Oliveira",
                    Latitude = 123,
                    Longitude = 456,
                    Foto = "123456",
                    Identificador = "maria.aparecida@gmail.com",
                    Rua = "Bougainville",
                    Bairro = "Residencial dos Ipês",
                    Cidade = "Goiânia",
                    Estado = "GO",
                    CpfCnpj = "451.123.586-45",
                    Telefone = "+5562991512536",
                    DataAtualizacao = DateTime.UtcNow,
                    TipoCadastro = Models.Enum.ETipoCadastro.Email,
                    TipoUsuario = Models.Enum.ETipoUsuario.Cliente
                }
            };
        }
    }
}