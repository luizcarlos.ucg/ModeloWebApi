﻿using System;

namespace Marmitinha.Models
{
    public class Pedido
    {
        public int Id { get; set; }
        public DateTime DataHoraFechamento { get; set; }
        public DateTime DataHoraEntrega { get; set; }
        public double LatitudeEntrega { get; set; }
        public double LongitudeEntrega { get; set; }
        public bool DeveEntregar { get; set; }
        public bool Cancelado { get; set; }
        public DateTime DataHoraGravacao { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public Enum.EAndamentoPedido AndamentoPedido { get; set; }
    }
}