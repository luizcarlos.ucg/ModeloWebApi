﻿using System;

namespace Marmitinha.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Foto { get; set; }
        public string Identificador { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string CpfCnpj { get; set; }
        public string Telefone { get; set; }
        public DateTime DataAtualizacao { get; set; }
        public Enum.ETipoUsuario TipoUsuario { get; set; }
        public Enum.ETipoCadastro TipoCadastro { get; set; }
    }
}