﻿using FluentValidation;

namespace Marmitinha.Models.Validators
{
    public class UsuarioValidator : AbstractValidator<Usuario>
    {
        public UsuarioValidator()
        {
            RuleFor(x => x.Nome).NotEmpty().Length(5, 50);
            RuleFor(x => x.CpfCnpj).NotEmpty();
        }
    }
}