﻿namespace Marmitinha.Models.Enum
{
    public enum EAndamentoPedido
    {
        AguardandoAprovRestaurante = 0,
        AprovadoRestaurante,
        ReprovadoRestaurante,
        PedidoPronto,
        PedidoACaminho,
        PedidoEntregue
    }
}