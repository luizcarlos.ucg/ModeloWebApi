﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marmitinha.Negocio
{
    public class Pedido
    {
        public async Task<List<Models.Pedido>> ObtenhaTodos()
        {
            return await new Persistencia.Pedido().ObtenhaTodos();
        }

        public async Task<Models.Pedido> Obtenha(int id)
        {
            return await new Persistencia.Pedido().Obtenha(id);
        }

        public async Task<int> Inclua(Models.Pedido pedido)
        {
            return await new Persistencia.Pedido().Inclua(pedido);
        }

        public async Task<bool> Altere(Models.Pedido usuario)
        {
            return await new Persistencia.Pedido().Altere(usuario);
        }

        public async Task<bool> Exclua(int id)
        {
            return await new Persistencia.Pedido().Exclua(id);
        }
    }
}