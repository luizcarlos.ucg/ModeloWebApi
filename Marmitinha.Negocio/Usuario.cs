﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marmitinha.Negocio
{
    public class Usuario
    {
        public async Task<List<Models.Usuario>> ObtenhaTodos()
        {
            return await new Persistencia.Usuario().ObtenhaTodos();
        }

        public async Task<Models.Usuario> Obtenha(int id)
        {
            return await new Persistencia.Usuario().Obtenha(id);
        }

        public async Task<int> Inclua(Models.Usuario usuario)
        {
            return await new Persistencia.Usuario().Inclua(usuario);
        }

        public async Task<bool> Altere(Models.Usuario usuario)
        {
            return await new Persistencia.Usuario().Altere(usuario);
        }

        public async Task<bool> Exclua(int id)
        {
            return await new Persistencia.Usuario().Exclua(id);
        }
    }
}