﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marmitinha.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class PedidoController : ControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<Models.Pedido>> Get()
        {
            return await new Negocio.Pedido().ObtenhaTodos();
        }

        [HttpGet("{id}")]
        public async Task<Models.Pedido> Get(int id)
        {
            return await new Negocio.Pedido().Obtenha(id);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Models.Pedido value)
        {
            ValidationResult result = new Models.Validators.PedidoValidator().Validate(value);

            if (!result.IsValid)
            {
                return BadRequest(result.Errors);
            }

            value.Id = await new Negocio.Pedido().Inclua(value);

            if(value.Id <= 0)
            {
                return BadRequest();
            }

            return CreatedAtAction("Get", new { id = value.Id }, value);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Models.Pedido value)
        {
            ValidationResult result = new Models.Validators.PedidoValidator().Validate(value);

            if (!result.IsValid)
            {
                return BadRequest(result.Errors);
            }

            if(!(await new Negocio.Pedido().Altere(value)))
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!(await new Negocio.Pedido().Exclua(id)))
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}