﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marmitinha.WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class UsuarioController : ControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<Models.Usuario>> Get()
        {
            return await new Negocio.Usuario().ObtenhaTodos();
        }

        [HttpGet("{id}")]
        public async Task<Models.Usuario> Get(int id)
        {
            return await new Negocio.Usuario().Obtenha(id);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Models.Usuario value)
        {
            ValidationResult result = new Models.Validators.UsuarioValidator().Validate(value);

            if (!result.IsValid)
            {
                return BadRequest(result.Errors);
            }

            value.Id = await new Negocio.Usuario().Inclua(value);

            if(value.Id <= 0)
            {
                return BadRequest();
            }

            return CreatedAtAction("Get", new { id = value.Id }, value);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Models.Usuario value)
        {
            ValidationResult result = new Models.Validators.UsuarioValidator().Validate(value);

            if (!result.IsValid)
            {
                return BadRequest(result.Errors);
            }

            if(!(await new Negocio.Usuario().Altere(value)))
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!(await new Negocio.Usuario().Exclua(id)))
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}